import time

from fixtures.chrome import chrome_browser
from fixtures.testarena.login import browser
from pages.admin_page import AdminPage
from pages.home_page import HomePage
from pages.project_page import ProjectPage
from pages.add_project_page import AddProjectPage
from fixtures.testarena.data_string import generate_random_string, random_string_data
from fixtures.testarena.wait_for_element import wait_for_element

administrator_email = 'administrator@testarena.pl'


def test_navigation_to_project_page(browser):
    home_page = HomePage(browser)
    project_page = home_page.go_to_project_page()
    assert project_page.is_loaded()


def test_add_project(browser):
    home_page = HomePage(browser)
    project_page = home_page.go_to_project_page()
    add_project_page = project_page.go_to_add_project_page()
    assert add_project_page.is_loaded()


def test_put_project_data(browser, generate_random_string, random_string_data):
    home_page = HomePage(browser)
    project_page = home_page.go_to_project_page()
    # add_project_page = project_page.go_to_add_project_page()
    project_page.go_to_add_project_page()
    add_project_page = AddProjectPage(browser)
    name = generate_random_string(10)
    prefix = generate_random_string(6)
    description = "długi opis na 2000 znaków"
    add_project_page.put_project_data(name, prefix, description)
    random_string_data['name'] = name
    random_string_data['prefix'] = prefix
    random_string_data['description'] = description
    assert project_page.is_loaded()


def test_project_is_added(browser):
    home_page = HomePage(browser)
    project_view_page = home_page.clik_project_icon_from_left_menu()
    # project_view_page.
    assert project_view_page.is_loaded()


def test_successful_login(browser):
    home_page = HomePage(browser)
    user_email = home_page.get_current_user_email()
    assert administrator_email == user_email


def test_open_admin_panel(browser):
    home_page = HomePage(browser)
    home_page.click_mail_icon()


def test_click_administration(browser):
    home_page = AdminPage(browser)
    home_page.click_administration()
