import pytest
import random
import string


@pytest.fixture
def random_string_data():
    return {}


@pytest.fixture
def generate_random_string():
    def generate_random_string_2(length=10):  # Funkcja wewnętrzna
        letters = string.ascii_letters
        return ''.join(random.choice(letters) for i in range(length))  # Zwracanie funkcji wewnętrznej

    return generate_random_string_2
