import pytest
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

@pytest.fixture
def wait_for_element():
    def _wait_for_element(browser, selector, timeout=10):
        try:
            WebDriverWait(browser, timeout).until(
                EC.presence_of_element_located(selector)
            )
            return True
        except:
            return False
    return _wait_for_element